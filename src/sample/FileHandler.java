package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nourhalabi on 27/03/16.
 */
public class FileHandler {

    private String file_text;
    public ArrayList<Files> tempFilesList;
    File[] filesInDir;

    public FileHandler () {
        file_text = "";
        tempFilesList = new ArrayList<>();
    }

    //_____Reads in the file and stores it in a string which gets returned_____//
    public String ReadtextFromFile(File file)throws IOException {

        if (file.exists()) {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                file_text += scanner.nextLine();
                file_text += "\r\n";
            }
        }

        else {
            System.out.println("File does not exist.");
        }

        Files files = new Files(file.getName(), file_text);
        tempFilesList.add(files);
        return file_text;
    }


    //_____Print text to output file_____//
    public void SaveTextToFile(String File_text, File outputFile)throws IOException {
        System.out.println("Saving text to " + outputFile.getAbsolutePath());
        if (!outputFile.exists() || outputFile.canWrite()) {
            PrintWriter fout = new PrintWriter(outputFile);

            fout.println(File_text);
            fout.close();
        } else {
            System.err.println("Cannot write to output file");

        }
    }

    //_____Get the list of files in the local folder_____//
    public String getDirectoryList(File file) throws IOException{
        String filenames = "";

        if (file.isDirectory()) {
            // process all of the files recursively
            filesInDir = file.listFiles();

            for (File f : filesInDir) {
                getDirectoryList(f);

            }
        }
        else if (file.exists())
            {

            for (int i = 0; i < filesInDir.length; i++) {

                filenames = "";
                System.out.println(file.getName());

                //get the names of the files
                filenames += file.getName();
                filenames += ","; //separator is comma


                File filee = file;
                if(filee.exists())
                {
                    Scanner scanner = new Scanner(filee);
                    file_text = "";
                    while (scanner.hasNextLine()) {
                        file_text += scanner.nextLine();
                        file_text += "\r\n";
                    }
                }
                Files files = new Files(filee.getName(), file_text);
                tempFilesList.add(files);
                return file_text;
            }
                filenames = "";
            }

        System.out.println(filenames);
        return filenames;
    }

    //_____Setup observable list of all files in local folder_____//
    public ObservableList<Files> getAllLocalFiles() {
        ObservableList<Files> filesList = FXCollections.observableArrayList();

        for(int i = 0; i < tempFilesList.size(); i++) {
            filesList.add(new Files(tempFilesList.get(i).getFilename(), tempFilesList.get(i).getText()));
        }

        return filesList;
    }

    //_____Setup observable list of all files in local folder_____//
    public ObservableList<Files> getAllSharedFiles(Client client) {
        ObservableList<Files> serverList = FXCollections.observableArrayList();

        client.getDirectorylist();

        for(int i =0; i < client.directorylist.size(); i ++)
        {
            serverList.add(new Files(client.directorylist.get(i),""));
            System.out.println(client.directorylist.get(i));
        }

       // String
        //for(int i=0; i < client.directorylist.size(); i++ ) {
        //    String filename [i] = client.directorylist.get(i);
        //}
       /* for(int i = 0; i < filename.length; i++) {
            serverList.add(new Files(filename[i], ""));
        }
        */

       // System.out.println(filename.length);

        return serverList;
    }

}
