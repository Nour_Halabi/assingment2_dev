package sample;

/**
 * Created by nourhalabi on 27/03/16.
 */
public class Files {

    private String filename;
    private String text;


    public Files (String Filename, String Text){
        this.filename = Filename;
        this.text = Text;
    }

    public String getFilename() {  return filename;  }

    public void setFilename(String filename) { this.filename = filename;  }

    public String getText() { return text;  }

    public void setText(String text) { this.text = text;  }

}
